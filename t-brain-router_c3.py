#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import zmq
import json
import numpy as np
import socket as udp_socket

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.setsockopt(zmq.SUBSCRIBE, "".encode('ascii'))
socket.connect('tcp://127.0.0.1:6003')
print ("socket connected")

UDP_IP = "172.16.11.228"
UDP_PORT = 9603

udp_sock = udp_socket.socket(udp_socket.AF_INET, udp_socket.SOCK_DGRAM) # UDP

TMPrevWork = np.load('/home/tera/t-brain/t-brain-router/data/TMPrevWork.npy')
while True:
    message = socket.recv_string()
    dataDict = json.loads(str(message))
    print ("--------------")
    print(dataDict)
    for b in dataDict["items"]:
        xMinPrev = float(b["xMinPrev"])
        xMaxPrev = float(b["xMaxPrev"])
        yMinPrev = float(b["yMinPrev"])
        yMaxPrev = float(b["yMaxPrev"])
        pcsClass = int(b["class"])
        pPrev = ( (xMinPrev + xMaxPrev) / 2.0 , (yMinPrev + yMaxPrev) / 2.0 )
        pPrevH = (pPrev[0] , pPrev[1] , 1.0)
        pWorkH = np.dot(TMPrevWork, pPrevH)
        pWork = (int((pWorkH[0] / pWorkH[2])), int((pWorkH[1] / pWorkH[2])))
        UDP_MESSAGE = str(str(int(pWork[0]))+","+str(int(pWork[1]))).encode("ascii")
        udp_sock.sendto ( UDP_MESSAGE, (UDP_IP, UDP_PORT))

