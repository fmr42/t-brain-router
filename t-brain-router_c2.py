#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import zmq
import json
import numpy as np
import socket as udp_socket

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.setsockopt(zmq.SUBSCRIBE, "".encode('ascii'))
socket.connect('tcp://127.0.0.1:6002')
print ("socket connected")

camImgWidth  = 1920 # image width of the realsense camera. Nominal: 1920
camImgHeight = 1080 # image height of the realsense camera. Nominal: 1080
netImgWidth  = 512
netImgHeight = 512

UDP_IP = "172.16.11.228"
UDP_PORT = 9602

udp_sock = udp_socket.socket(udp_socket.AF_INET, udp_socket.SOCK_DGRAM) # UDP

classes = ['background', 'connector', 'electrolytic', 'inductor', 'ceramic']

TMPrevWork = np.load('/home/tera/t-brain/t-brain-router/data/TMPrevWork.npy')
while True:
    message = socket.recv_string()
    dataDict = json.loads(str(message))
    print ("--------------")
    print(dataDict)
    for b in dataDict["items"]:
        xMinPrev = float(b["xMinPrev"])
        xMaxPrev = float(b["xMaxPrev"])
        yMinPrev = float(b["yMinPrev"])
        yMaxPrev = float(b["yMaxPrev"])
        pcsClass = int(b["class"])
        pPrev = ( (xMinPrev + xMaxPrev) / 2.0 , (yMinPrev + yMaxPrev) / 2.0 )
        pPrevH = (pPrev[0] , pPrev[1] , 1.0)
        pWorkH = np.dot(TMPrevWork, pPrevH)
        pWork = (int((pWorkH[0] / pWorkH[2])), int((pWorkH[1] / pWorkH[2])))
        UDP_MESSAGE = str(str(int(pWork[0]))+","+str(int(pWork[1]))).encode("ascii")
        print(str(pcsClass) + " " +  str(classes[pcsClass]) + " " + str(UDP_MESSAGE))
        udp_sock.sendto ( UDP_MESSAGE, (UDP_IP, UDP_PORT))


